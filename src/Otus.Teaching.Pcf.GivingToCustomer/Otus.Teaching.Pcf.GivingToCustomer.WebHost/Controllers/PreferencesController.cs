﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IRepository<Preference> preferencesRepository,
                                     IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            try
            {
                var res = await _distributedCache.GetStringAsync("PreferenceResponse");
                if (res != null)
                {
                    var cached = JsonSerializer.Deserialize<List<PreferenceResponse>>(res);
                    return Ok(cached);
                }
            }
            catch (Exception)
            {
            }

            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            try
            {
                await _distributedCache.SetStringAsync(
                    key: "PreferenceResponse",
                    value: JsonSerializer.Serialize(response),
                    options: new DistributedCacheEntryOptions
                    {
                        SlidingExpiration = TimeSpan.FromHours(1)
                    });
            }
            catch (Exception)
            {
            }
            
            return Ok(response);
        }
    }
}